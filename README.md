# README #


### Overview ###

* This is a repository for Python scripts which read sensor data from GPIO pins on a Raspberry Pi.
* Sensors will monitor conditions inside a Freshwater Aquarium.
* Initial set-up will include scripts to read temperature and PH levels.
